import { Client } from '@elastic/elasticsearch';

export default (opts) => ({
  created() {
    const {
      protocol = 'http',
      host,
      port = null,
      username,
      password,
      log = 'tracing',
    } = opts;

    this.elasticSearch = new Client({
      node: port ? `${protocol}://${host}:${port}` : `${protocol}://${host}`,
      auth: {
        username,
        password,
      },
      log,
    });
  },
});
