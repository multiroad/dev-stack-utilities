import EventEmitter from 'events';

export default class LocalTransaction extends EventEmitter {
  constructor(id, master = false) {
    super();
    this._id = id;
    this._status = 'exec';
    this._isMaster = master;
  }

  get status() {
    return this._status;
  }

  commit() {
    this._status = 'committed';
    return this.emit('commit');
  }

  rollback() {
    this._status = 'rolled_back';
    return this.emit('rollback');
  }

  get id() {
    return this._id;
  }

  get committed() {
    return this._status === 'committed';
  }

  get rolledBack() {
    return this._status === 'rolled_back';
  }

  get isMaster() {
    return this._isMaster;
  }
}
