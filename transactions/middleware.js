import Redis from 'ioredis';
import Transaction from './Transaction.class';
import _ from 'lodash';

export default (createTransaction) => {
  return (broker) => {
    const logger = broker.getLogger('transactions');

    function runCompensator({ opts, action, ctx, transaction }) {
      return opts.compensator(ctx)
        .then(() => {
          logger.debug(`Action '${action.name}': Successfully handled compensator (tr_id: '${transaction && transaction.id || 'null'}', ctx_id: '${ctx.id}')`);
        })
        .catch(err => {
          logger.error(`Action '${action.name}': Compensator handler failed (tr_id: '${transaction && transaction.id || 'null'}', ctx_id: '${ctx.id}')`, err);
          throw err;
        });
    }

    function commitLocalTransaction({ localTransaction, action, transaction, ctx }) {
      if (localTransaction.finished === 'commit') {
        logger.debug(`Action '${action.name}': Local transaction was already committed (tr_id: '${transaction && transaction.id || 'null'}', ctx_id: '${ctx.id}')`);
        return Promise.resolve();
      }
      if (localTransaction.finished === 'rollback') {
        logger.warn(`Action '${action.name}': Local transaction was already rolled back but trying to commit (tr_id: '${transaction && transaction.id || 'null'}', ctx_id: '${ctx.id}')`);
        return Promise.resolve();
      }
      return localTransaction.commit()
        .then(() => {
          logger.debug(`Action '${action.name}': Successfully committed local transaction (tr_id: '${transaction && transaction.id || 'null'}', ctx_id: '${ctx.id}')`);
        })
        .catch(err => {
          logger.error(`Action '${action.name}': Committing local transaction for action has failed (tr_id: '${transaction && transaction.id || 'null'}', ctx_id: '${ctx.id}')`, err);
          throw err;
        });
    }

    function rollbackLocalTransaction({ localTransaction, action, transaction, ctx }) {
      if (localTransaction.finished === 'rollback') {
        logger.debug(`Action '${action.name}': Local transaction was already rolled back (tr_id: '${transaction && transaction.id || 'null'}', ctx_id: '${ctx.id}')`);
        return Promise.resolve();
      }
      if (localTransaction.finished === 'commit') {
        logger.warn(`Action '${action.name}': Local transaction was already committed but trying to rollback (tr_id: '${transaction && transaction.id || 'null'}', ctx_id: '${ctx.id}')`);
        return Promise.resolve();
      }
      return localTransaction.rollback()
        .then(() => {
          logger.debug(`Action '${action.name}': Successfully rolled back local transaction (tr_id: '${transaction && transaction.id || 'null'}', ctx_id: '${ctx.id}')`);
        })
        .catch(err => {
          logger.error(`Action '${action.name}': Rolling back local transaction has failed (tr_id: '${transaction && transaction.id || 'null'}', ctx_id: '${ctx.id}')`, err);
          throw err;
        });
    }

    const sub = new Redis(process.env.LOCAL_REDIS ? {
      host: process.env.TRANSACTIONS_PUB_SUB_REDIS_HOST,
      port: process.env.TRANSACTIONS_PUB_SUB_REDIS_PORT,
    } : {
      sentinels: [
        {
          host: process.env.TRANSACTIONS_PUB_SUB_REDIS_HOST,
          port: process.env.TRANSACTIONS_PUB_SUB_REDIS_PORT,
        },
      ],
      name: 'mymaster',
    });

    const pub = new Redis(process.env.LOCAL_REDIS ? {
      host: process.env.TRANSACTIONS_PUB_SUB_REDIS_HOST,
      port: process.env.TRANSACTIONS_PUB_SUB_REDIS_PORT,
    } : {
      sentinels: [
        {
          host: process.env.TRANSACTIONS_PUB_SUB_REDIS_HOST,
          port: process.env.TRANSACTIONS_PUB_SUB_REDIS_PORT,
        },
      ],
      name: 'mymaster',
    });

    sub.subscribe('transactions', () => {
      logger.info('Successfully subscribed to \'transactions\' channel');
    });

    const transactionControllers = {};
    const localTransactions = {};

    sub.on('message', (channel, message) => {
      if (channel !== 'transactions') return;
      const [transaction_id, event] = message.split(':');

      if (!transactionControllers[transaction_id]) {
        logger.debug(`Received transaction (tr_id: '${transaction_id}') '${event}' event, but no subscribers exists. Skipping`);
        return;
      }

      switch (event) {
        case 'commit':
          transactionControllers[transaction_id].forEach((transaction) => {
            transaction.commit();
            transaction.removeAllListeners();
          });
          delete transactionControllers[transaction_id];
          break;
        case 'rollback':
          transactionControllers[transaction_id].forEach((transaction) => {
            transaction.rollback();
            transaction.removeAllListeners();
          });
          delete transactionControllers[transaction_id];
          break;
        default:
          logger.error(`Received invalid event ('${event}') for transaction (tr_id: '${transaction_id}')`);
      }
    });

    return {
      localAction(handler, action) {
        const opts = _.defaultsDeep({}, _.isObject(action.transaction) ? action.transaction : { enabled: !!action.transaction });

        if (opts.compensator) {
          // Оборачиваем компенсатор в Promise
          opts.compensator = broker.Promise.method(opts.compensator.bind(action.service));
        }

        return (ctx) => {
          /**
           * вызываем обработчик как обычно
           */
          if (!ctx.meta.$transaction_id && !opts.enabled) {
            return handler(ctx)
              .then((response) => {
                /**
                 * Если экшен вызван не в контексте транзакции
                 * и транзакции у него выключены,
                 * но он вернул локальную транзакцию - выбрасываем ошибку
                 */
                if (ctx.locals.$transaction) {
                  throw new Error(`Action '${action.name}': Called not in distributed transaction but has returned local transaction. Local transaction will be rolled back.`);
                }

                return response;
              })
              .catch(async (err) => {
                if (ctx.locals.$transaction) {
                  /**
                   * При наступлении ошибки
                   * откатываем локальную транзакцию,
                   * если она есть
                   */
                  await rollbackLocalTransaction({
                    localTransaction: ctx.locals.$transaction,
                    action,
                    ctx,
                  });
                }

                throw err;
              });
          }

          // Флаг, указывающий, что текущая транзакция является мастером
          let master = false;

          if (opts.enabled) {
            if (!ctx.meta.$transaction_id) {
              /**
               * Если транзакции включены для данного экшена,
               * но он вызван не в контексте распределённой транзакции -
               * генерируем новый id распределённой транзакции
               */
              ctx.meta.$transaction_id = broker.generateUid();
              master = true;
            }
          }

          // Создаём объект распределённой транзакции
          const transaction = new Transaction(ctx.meta.$transaction_id, master);

          if (!transactionControllers[transaction.id]) {
            transactionControllers[transaction.id] = [];
          }

          // Добавляем его в набор обработчиков
          transactionControllers[transaction.id].push(transaction);

          if (transaction.isMaster) {
            logger.debug(`Action '${action.name}': Started new distributed transaction (tr_id: '${transaction.id}', ctx_id: '${ctx.id}')`);
          } else {
            logger.debug(`Action '${action.name}': Participates in distributed transaction (tr_id: '${transaction.id}', ctx_id: '${ctx.id}')`);
          }

          return handler(ctx)
            .then(async response => {
              if (transaction.isMaster) {
                /**
                 * Если экшен распределённой мастер-транзакции успешно завершился -
                 * отправляем событие коммита распределённой транзакции
                 */
                logger.debug(`Action '${action.name}': Distributed transaction completed successfully. Sending commit event (tr_id: '${transaction.id}', ctx_id: '${ctx.id}')`);
                await pub.publish('transactions', [transaction.id, 'commit', ctx.action.name].join(':'));

                /**
                 * Если экшен распределённой мастер-транзакции
                 * вернул объект локальной транзакции - коммитим её
                 */
                if (ctx.locals.$transaction) {
                  await commitLocalTransaction({
                    localTransaction: ctx.locals.$transaction,
                    action,
                    ctx,
                    transaction,
                  });
                }

                /**
                 * Удаляем id транзакции из контекста, чтобы он не просочился в следующие подвызовы
                 */
                delete ctx.meta.$transaction_id;

                return response;
              }

              if (ctx.locals.$transaction) {
                /**
                 * Если экшен распределённой транзакции успешно завершился,
                 * и вернул объект локальной транзакции
                 */
                const localTransaction = ctx.locals.$transaction;

                if (transaction.committed) {
                  /**
                   * Коммитим локальную транзакцию,
                   * если распределённая транзакция уже закоммичена
                   * (В теории не должно случиться никогда)
                   */
                  logger.warn('Distributed transaction committed before local action finished!');
                  await commitLocalTransaction({
                    localTransaction,
                    action,
                    ctx,
                    transaction,
                  });
                } else {
                  /**
                   * Подписываемся на событие коммита распределённой транзакции
                   * и коммитим локальную транзакцию при наступлении такого события
                   */
                  transaction.on('commit', () => commitLocalTransaction({
                    localTransaction,
                    action,
                    ctx,
                    transaction,
                  }));
                }

                if (transaction.rolledBack) {
                  /**
                   * Откатываем локальную транзакцию,
                   * если распределённая транзакция была провалена
                   * до того, как текущий экшен закончил работу
                   * (когда один из параллельных вызовов был провален)
                   */
                  await rollbackLocalTransaction({
                    localTransaction,
                    action,
                    ctx,
                    transaction,
                  });
                } else {
                  /**
                   * Подписываемся на событие отката распределённой транзакции
                   * и откатываем локальную транзакцию при наступлении такого события
                   */
                  transaction.on(
                    'rollback',
                    () => rollbackLocalTransaction({
                      localTransaction,
                      action,
                      ctx,
                      transaction,
                    }),
                  );
                }
              } else if (opts.compensator) {
                /**
                 * Если у обработчика экшена есть компенсатор
                 */
                if (transaction.rolledBack) {
                  /**
                   * Запускаем компенсатор
                   * если распределённая транзакция была провалена
                   * до того, как текущий экшен закончил работу
                   * (когда один из параллельных вызовов был провален)
                   */
                  await runCompensator({ opts, action, ctx, transaction });
                } else {
                  /**
                   * Подписываемся на событие отката распределённой транзакции
                   * и откатываем локальную транзакцию при наступлении такого события
                   */
                  transaction.on(
                    'rollback',
                    () => runCompensator({ opts, action, ctx, transaction }),
                  );
                }
              } else {
                logger.warn(`Action '${action.name}': Called in distributed transaction, but has no compensator and did not return a local transaction. (tr_id: '${transaction.id}', ctx_id: '${ctx.id}')`);
              }

              return response;
            })
            .catch(async err => {
              if (transaction.isMaster) {
                /**
                 * Если экшен распределённой мастер-транзакции провалился -
                 * отправляем событие отката распределённой транзакции
                 */
                logger.debug(`Action '${action.name}': Distributed transaction failed. Sending rollback event (tr_id: '${transaction.id}', ctx_id: '${ctx.id}')`);
                await pub.publish('transactions', [transaction.id, 'rollback', ctx.action.name].join(':'));
              }

              if (ctx.locals.$transaction) {
                /**
                 * Если экшен распределённой транзакции провалился
                 * и вернул объект локальной транзакции - откатываем её
                 */
                await rollbackLocalTransaction({
                  localTransaction: ctx.locals.$transaction,
                  action,
                  ctx,
                  transaction,
                });
              } else if (opts.compensator) {
                /**
                 * Если экшен распределённой транзакции провалился
                 * и имеет компенасатор - вызываем компенсатор
                 */
                await runCompensator({ opts, action, ctx, transaction });
              } else {
                logger.warn(`Action '${action.name}': Called and failed in distributed transaction, but has no compensator and did not return a local transaction. (tr_id: '${transaction.id}', ctx_id: '${ctx.id}')`);
              }

              throw err;
            });
        };
      },
      serviceCreating(service, schema) {
        if (!schema.methods) {
          schema.methods = {};
        }

        schema.methods.$resolveTransaction = async function $resolveTransaction(ctx) {
          if (!ctx.meta.$transaction_id) {
            throw new Error(`Action: '${ctx.action.name}': $resolveTransaction called not in context of distributed transaction!`);
          }

          if (!localTransactions[ctx.meta.$transaction_id]) {
            localTransactions[ctx.meta.$transaction_id] = new Map();
          }

          if (!localTransactions[ctx.meta.$transaction_id].get(this)) {
            localTransactions[ctx.meta.$transaction_id].set(this, await createTransaction.call(this, this));
            logger.debug(`Action: '${ctx.action.name}': $resolveTransaction created new local transaction`);
          } else {
            logger.debug(`Action: '${ctx.action.name}': $resolveTransaction returned existing local transaction`);
          }
          ctx.locals.$transaction = localTransactions[ctx.meta.$transaction_id].get(this);

          return localTransactions[ctx.meta.$transaction_id].get(this);
        };
      },
      // call(next) {
      //   return function (actionName, params, opts) {
      //     console.log('The \'call\' is called.', actionName);
      //
      //     if (opts.parentCtx && opts.parentCtx.meta.$transaction_id && actionName.startsWith(opts.parentCtx.action.service)) {
      //
      //     }
      //
      //     return next(actionName, params, opts).then(res => {
      //       console.log('Response:', res);
      //       return res;
      //     });
      //   };
      // },
    };
  };
};
