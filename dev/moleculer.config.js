import molConfigMaker from '../moleculer-config';
import TransactionsMiddleware from '../moleculer-config/middlewares/Transactions';

export default molConfigMaker({
  nodeID: `project-utils-${String(+new Date()).slice(-5)}`,
  transporter: {
    options: {
      url: process.env.NATS_URL,
      user: process.env.NATS_USERNAME,
      pass: process.env.NATS_PASSWORD,
    },
  },
  cacher: process.env.CACHE_ENGINE === 'Redis' ? {
    type: 'Redis',
    options: {
      redis: process.env.LOCAL_REDIS ? {
        host: process.env.CACHE_REDIS_HOST,
        port: process.env.CACHE_REDIS_PORT,
      } : {
        sentinels: [
          {
            host: process.env.CACHE_REDIS_HOST,
            port: process.env.CACHE_REDIS_PORT,
          },
        ],
        name: 'mymaster',
      },
    },
  } : 'Memory',
  middlewares: [TransactionsMiddleware],
});
