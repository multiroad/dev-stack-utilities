export default {
  name: 'getter',
  actions: {
    async small(ctx) {
      console.time(ctx.params.name);
      const res = await ctx.call('source.small');
      console.timeEnd(ctx.params.name);
    },
    async big(ctx) {
      console.time(ctx.params.name);
      const res = await ctx.call('source.big');
      console.timeEnd(ctx.params.name);
    },
  },
  methods: {},
  started() {
    setTimeout(async () => {
      for (let i = 0; i < 1; i++) {
        // this.broker.call('getter.small', {
        //   name: `small-${i}`,
        // });
        this.broker.call('getter.big', {
          name: `big-${i}`,
        });
      }
      // await this.broker.call('getter.big');
    }, 5000);
  },
};
