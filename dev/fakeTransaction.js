export default class FakeTransaction {
  constructor(obj, prop, ctx) {
    this.obj = obj;
    this.prop = prop;
    this.initValue = this.obj[prop];
    this.ctx = ctx;
  }

  async update(value) {
    if (!this.obj[this.prop]) throw Error('No Prop in object');
    this.obj[this.prop] = value;
  }

  async commit() {
    // console.log('Transaction committed', this.ctx.action.name, this.ctx.params);
  }

  async rollback() {
    this.obj[this.prop] = this.initValue;
    // console.log('Transaction rolled back', this.ctx.action.name, this.ctx.params);
  }
}
