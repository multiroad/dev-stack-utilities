/* eslint-disable import/no-extraneous-dependencies */
import _ from 'lodash';
import Promise from 'bluebird';
import OfficialMixin from 'moleculer-db';
import { EntityNotFoundError } from 'moleculer-db/src/errors';
import { Errors } from 'moleculer';

export default {
  mixins: [OfficialMixin],
  actions: {
    /**
     * Create many new entities updating duplicates.
     *
     * @actions
     *
     * @param {Object?} entity - Entity to save.
     * @param {Array<Object>?} entities - Entities to save.
     *
     * @returns {Object|Array<Object>} Saved entity(ies).
     */
    upsert: {
      params: {
        entity: { type: 'object', optional: true },
        entities: { type: 'array', optional: true },
        fields: {
          type: 'array',
          optional: true,
        },
      },
      handler(ctx) {
        return this._upsert(ctx, ctx.params);
      },
    },
  },
  methods: {
    transformSort(paramSort) {
      let sort = paramSort;
      if (_.isString(sort)) {
        sort = sort.replace(/,/, ' ').split(' ');
      }

      if (Array.isArray(sort)) {
        let sortObj = [];
        sort.forEach(s => {
          if (s.startsWith('-')) {
            sortObj.push([s.slice(1), 'DESC']);
          } else {
            sortObj.push([s, 'ASC']);
          }
        });
        return sortObj;
      }

      if (_.isObject(sort)) {
        return Object.keys(sort).map(name => [name, sort[name] > 0 ? 'ASC' : 'DESC']);
      }

      /* istanbul ignore next*/
      return [];
    },
    sanitizeParams(ctx, params) {
      let p = Object.assign({}, params);

      // Convert from string to number
      if (typeof (p.limit) === 'string') {
        p.limit = Number(p.limit);
      }
      if (typeof (p.offset) === 'string') {
        p.offset = Number(p.offset);
      }
      if (typeof (p.page) === 'string') {
        p.page = Number(p.page);
      }
      if (typeof (p.pageSize) === 'string') {
        p.pageSize = Number(p.pageSize);
      }
      // Convert from string to POJO
      if (typeof (p.query) === 'string') {
        p.query = JSON.parse(p.query);
      }

      if (typeof (p.calculationsQuery) === 'string') {
        p.calculationsQuery = JSON.parse(p.calculationsQuery);
      }

      if (typeof (p.sort) === 'string') {
        p.sort = p.sort.replace(/,/g, ' ').split(' ');
      }

      if (typeof (p.fields) === 'string') {
        p.fields = p.fields.replace(/,/g, ' ').split(' ');
      }

      if (typeof (p.populate) === 'string') {
        p.populate = p.populate.replace(/,/g, ' ').split(' ');
      }

      if (typeof (p.searchFields) === 'string') {
        p.searchFields = p.searchFields.replace(/,/g, ' ').split(' ');
      }

      if (!p.include) {
        p.include = [];
      }


      if (ctx.action.name.endsWith('.list')) {
        // Default `pageSize`
        if (!p.pageSize) {
          p.pageSize = this.settings.pageSize;
        }

        // Default `page`
        if (!p.page) {
          p.page = 1;
        }

        // Limit the `pageSize`
        if (this.settings.maxPageSize > 0 && p.pageSize > this.settings.maxPageSize) {
          p.pageSize = this.settings.maxPageSize;
        }


        // Calculate the limit & offset from page & pageSize
        p.limit = p.pageSize;
        p.offset = (p.page - 1) * p.pageSize;
      }
      // Limit the `limit`
      if (this.settings.maxLimit > 0 && p.limit > this.settings.maxLimit) {
        p.limit = this.settings.maxLimit;
      }

      return p;
    },
    populateDocs(ctx, docs, populateFields) {
      if (!this.settings.populates || !Array.isArray(populateFields) || populateFields.length === 0) return Promise.resolve(docs);

      if (docs == null || (!_.isObject(docs) && !Array.isArray(docs))) return Promise.resolve(docs);

      const promises = [];
      _.forIn(this.settings.populates, (rule, field) => {
        if (populateFields.indexOf(field) === -1) return; // skip

        // if the rule is a function, save as a custom handler
        if (_.isFunction(rule)) {
          rule = {
            handler: Promise.method(rule),
          };
        }

        // If string, convert to object
        if (_.isString(rule)) {
          rule = {
            action: rule,
          };
        }
        rule.field = rule.field || field;
        const arr = Array.isArray(docs) ? docs : [docs];

        // Collect IDs from field of docs (flatten, compact & unique list)
        const idList = _.uniq(_.flattenDeep(_.compact(arr.map(doc => doc[rule.field]))));
        // Replace the received models according to IDs in the original docs
        const resultTransform = (populatedDocs) => {
          arr.forEach((doc) => {
            const id = doc[rule.field];
            if (_.isArray(id)) {
              const models = _.compact(id.map(id => populatedDocs[id]));
              doc[field] = models;
            } else {
              doc[field] = populatedDocs[id];
            }
          });
        };

        if (rule.handler) {
          promises.push(rule.handler.call(this, idList, arr, rule, ctx).then(resultTransform));
        } else if (idList.length > 0) {
          // Call the target action & collect the promises
          const params = Object.assign(
            {
              id: idList,
              mapping: true,
              populate: rule.populate,
            },
            rule.params || {},
          );

          promises.push(ctx.call(rule.action, params).then(resultTransform));
        }
      });

      return Promise.all(promises).then(() => docs);
    },
    /**
     * Get entity(ies) by ID(s).
     *
     * @methods
     * @param {any|Array<any>} id - ID or IDs.
     * @param {Boolean?} decoding - Need to decode IDs.
     * @returns {Object|Array<Object>} Found entity(ies).
     */
    getById(id, decoding, transaction) {
      return Promise.resolve()
        .then(() => {
          if (_.isArray(id)) {
            return this.adapter.findByIds(decoding ? id.map(id => this.decodeID(id)) : id, transaction);
          } else {
            return this.adapter.findById(decoding ? this.decodeID(id) : id, transaction);
          }
        });
    },

    /**
     * Find entities by query.
     *
     * @methods
     *
     * @param {Context} ctx - Context instance.
     * @param {Object?} params - Parameters.
     *
     * @returns {Array<Object>} List of found entities.
     */
    async _find(ctx, params) {
      const transaction = await this._resolveTransaction(ctx);

      return this.adapter.find(params, transaction)
        .then(docs => this.transformDocuments(ctx, params, docs));
    },

    /**
     * Get count of entities by query.
     *
     * @methods
     *
     * @param {Context} ctx - Context instance.
     * @param {Object?} params - Parameters.
     *
     * @returns {Number} Count of found entities.
     */
    async _count(ctx, params) {
      const transaction = await this._resolveTransaction(ctx);
      // Remove pagination params
      if (params && params.limit) {
        params.limit = null;
      }
      if (params && params.offset) {
        params.offset = null;
      }
      return this.adapter.count(params, transaction);
    },

    /**
     * List entities by filters and pagination results.
     *
     * @methods
     *
     * @param {Context} ctx - Context instance.
     * @param {Object?} params - Parameters.
     *
     * @returns {Object} List of found entities and count.
     */
    async _list(ctx, params) {
      const transaction = await this._resolveTransaction(ctx);

      let countParams = Object.assign({}, params);
      // Remove pagination params
      if (countParams && countParams.limit) {
        countParams.limit = null;
      }
      if (countParams && countParams.offset) {
        countParams.offset = null;
      }
      if (params.limit == null) {
        if (this.settings.limit > 0 && params.pageSize > this.settings.limit) {
          params.limit = this.settings.limit;
        } else {
          params.limit = params.pageSize;
        }
      }

      return Promise.all([
        // Get rows
        this.adapter.find(params, transaction),
        // Get count of all rows
        this.adapter.count(countParams, transaction),
      ]).then(res => {
        return this.transformDocuments(ctx, params, res[0])
          .then(docs => {
            return {
              // Rows
              rows: docs,
              // Total rows
              total: res[1],
              // Page
              page: params.page,
              // Page size
              pageSize: params.pageSize,
              // Total pages
              totalPages: Math.floor((res[1] + params.pageSize - 1) / params.pageSize),
            };
          });
      });
    },

    /**
     * Create a new entity.
     *
     * @methods
     *
     * @param {Context} ctx - Context instance.
     * @param {Object?} params - Parameters.
     *
     * @returns {Object} Saved entity.
     */
    async _create(ctx, entity) {
      const transaction = await this._resolveTransaction(ctx);

      return this.validateEntity(entity)
        // Apply idField
        .then(entity => this.adapter.beforeSaveTransformID(entity, this.settings.idField))
        .then(entity => this.adapter.insert(entity, transaction))
        .then(doc => (console.time('TRDOC'), doc))
        .then(doc => this.transformDocuments(ctx, {}, doc))
        .then(json => this.entityChanged('created', json, ctx).then(() => json));
    },

    /**
     * Create many new entities.
     *
     * @methods
     *
     * @param {Context} ctx - Context instance.
     * @param {Object?} params - Parameters.
     *
     * @returns {Object|Array.<Object>} Saved entity(ies).
     */
    async _insert(ctx, params) {
      const transaction = await this._resolveTransaction(ctx);

      return Promise.resolve()
        .then(() => {
          if (Array.isArray(params.entities)) {
            return this.validateEntity(params.entities)
              // Apply idField
              .then(entities => {
                if (this.settings.idField === '_id') {
                  return entities;
                }
                return entities.map(entity => this.adapter.beforeSaveTransformID(entity, this.settings.idField));
              })
              .then(entities => this.adapter.insertMany(entities, transaction));
          } else if (params.entity) {
            return this.validateEntity(params.entity)
              // Apply idField
              .then(entity => this.adapter.beforeSaveTransformID(entity, this.settings.idField))
              .then(entity => this.adapter.insert(entity, transaction));
          }
          return Promise.reject(new Errors.MoleculerClientError('Invalid request! The \'params\' must contain \'entity\' or \'entities\'!', 400));
        })
        .then(docs => this.transformDocuments(ctx, {}, docs))
        .then(json => this.entityChanged('created', json, ctx).then(() => json));
    },
    /**
     * Create many new entities updating duplicates.
     *
     * @methods
     *
     * @param {Context} ctx - Context instance.
     * @param {Object?} params - Parameters.
     *
     * @returns {Object|Array.<Object>} Saved entity(ies).
     */
    async _upsert(ctx, params) {
      const transaction = await this._resolveTransaction(ctx);

      return Promise.resolve()
        .then(() => {
          if (Array.isArray(params.entities)) {
            return this.validateEntity(params.entities)
              // Apply idField
              .then(entities => {
                if (this.settings.idField === '_id') {
                  return entities;
                }
                return entities.map(entity => this.adapter.beforeSaveTransformID(entity, this.settings.idField));
              })
              .then(entities => this.adapter.upsertMany(entities, params.fields, transaction));
          } else if (params.entity) {
            return this.validateEntity(params.entity)
              // Apply idField
              .then(entity => this.adapter.beforeSaveTransformID(entity, this.settings.idField))
              .then(entity => this.adapter.upsert(entity, params.fields, transaction));
          }
          return Promise.reject(new Errors.MoleculerClientError('Invalid request! The \'params\' must contain \'entity\' or \'entities\'!', 400));
        })
        .then(docs => this.transformDocuments(ctx, {}, docs))
        .then(json => this.entityChanged('created', json, ctx).then(() => json));
    },

    /**
     * Get entity by ID.
     *
     * @methods
     *
     * @param {Context} ctx - Context instance.
     * @param {Object?} params - Parameters.
     *
     * @returns {Object|Array<Object>} Found entity(ies).
     *
     * @throws {EntityNotFoundError} - 404 Entity not found
     */
    async _get(ctx, params) {
      const transaction = await this._resolveTransaction(ctx);

      let id = params.id;
      let origDoc;
      return this.getById(id, true, transaction)
        .then(doc => {
          if (!doc) {
            return Promise.reject(new EntityNotFoundError(id));
          }
          origDoc = doc;
          return this.transformDocuments(ctx, params, doc);
        })
        .then(json => {
          if (_.isArray(json) && params.mapping === true) {
            let res = {};
            json.forEach((doc, i) => {
              const id = this.adapter.afterRetrieveTransformID(origDoc[i], this.settings.idField)[this.settings.idField];
              res[id] = doc;
            });
            return res;
          } else if (_.isObject(json) && params.mapping === true) {
            let res = {};
            const id = this.adapter.afterRetrieveTransformID(origDoc, this.settings.idField)[this.settings.idField];
            res[id] = json;
            return res;
          }
          return json;
        });
    },

    /**
     * Update an entity by ID.
     * > After update, clear the cache & call lifecycle events.
     *
     * @methods
     *
     * @param {Context} ctx - Context instance.
     * @param {Object?} params - Parameters.
     * @returns {Object} Updated entity.
     *
     * @throws {EntityNotFoundError} - 404 Entity not found
     */
    async _update(ctx, params) {
      const transaction = await this._resolveTransaction(ctx);

      let id;
      let sets = {};
      // Convert fields from params to "$set" update object
      Object.keys(params).forEach(prop => {
        if (prop == 'id' || prop == this.settings.idField) {
          id = this.decodeID(params[prop]);
        } else {
          sets[prop] = params[prop];
        }
      });

      if (this.settings.useDotNotation) {
        sets = flatten(sets, { safe: true });
      }

      return this.adapter.updateById(id, { '$set': sets }, transaction)
        .then(doc => {
          if (!doc) {
            return Promise.reject(new EntityNotFoundError(id));
          }
          return this.transformDocuments(ctx, {}, doc)
            .then(json => this.entityChanged('updated', json, ctx).then(() => json));
        });
    },

    /**
     * Remove an entity by ID.
     *
     * @methods
     *
     * @param {Context} ctx - Context instance.
     * @param {Object?} params - Parameters.
     *
     * @throws {EntityNotFoundError} - 404 Entity not found
     */
    async _remove(ctx, params) {
      const transaction = await this._resolveTransaction(ctx);

      const id = this.decodeID(params.id);
      return this.adapter.removeById(id, transaction)
        .then(doc => {
          if (!doc) {
            return Promise.reject(new EntityNotFoundError(params.id));
          }
          return this.transformDocuments(ctx, {}, doc)
            .then(json => this.entityChanged('removed', json, ctx).then(() => json));
        });
    },

    _resolveTransaction(ctx) {
      return typeof this.$resolveTransaction === 'function' && ctx.meta.$transaction_id
        ? this.$resolveTransaction(ctx)
        : undefined;
    },
  },
};
