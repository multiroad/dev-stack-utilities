/*
 * moleculer-apollo-server
 * Copyright (c) 2018 MoleculerJS (https://github.com/moleculerjs/moleculer-apollo-server)
 * MIT Licensed
 */
const config = require('project-utils/project-config');
const _ = require('lodash');
const { MoleculerServerError } = require('moleculer').Errors;
const DataLoader = require('dataloader');
const { makeExecutableSchema } = require('graphql-tools');
const GraphQL = require('graphql');
const { PubSub, withFilter } = require('graphql-subscriptions');

// const {
//   PostgresPubSub: PubSub,
//   withFilter,
// } = require('graphql-postgres-subscriptions');

// const {
//   RedisPubSub: PubSub,
//   withFilter,
// } = require('graphql-redis-subscriptions');

const { ApolloServer } = require('./ApolloServer');

module.exports = function CreateService(mixinOptions) {
  mixinOptions = _.defaultsDeep(mixinOptions, {
    routeOptions: {
      path: '/graphql',
    },
    schema: null,
    serverOptions: {},
    createAction: true,
    subscriptionEventName: 'graphql.publish',
    meta: () => ({}),
  });

  let shouldUpdateSchema = true;

  const serviceSchema = {
    events: {
      '$services.changed': function () {
        if (config.HOT_RELOAD) {
          this.invalidateGraphQLSchema();
        }
      },
      [mixinOptions.subscriptionEventName](event) {
        this.pubsub.publish(event.tag, event.payload);
      },
    },
    methods: {
      /**
       * Invalidate the generated GraphQL schema
       */
      invalidateGraphQLSchema() {
        if (!shouldUpdateSchema) {
          this.logger.info('Graphql Schema was invalidated');
          this.server = null;
          this.createServer();
        }
        shouldUpdateSchema = true;
      },

      /**
       * Get action name for resolver
       *
       * @param {String} service
       * @param {String} action
       */
      getResolverActionName(service, action) {
        if (action.indexOf('.') === -1) {
          return `${service}.${action}`;
        }
        return action;
      },

      /**
       * Create resolvers for actions.
       *
       * @param {String} serviceName
       * @param {Object} resolvers
       */
      createActionResolvers(serviceName, resolvers) {
        const res = {};
        _.forIn(resolvers, (r, name) => {
          if (_.isString(r)) {
            // If String, it is an action name
            res[name] = this.createActionResolver(
              this.getResolverActionName(serviceName, r),
            );
          } else if (_.isPlainObject(r)) {
            // If Object, it is a remote action resolver
            if (r.type === 'function') {
              res[name] = eval(r.resolver);
            } else {
              res[name] = this.createActionResolver(
                this.getResolverActionName(serviceName, r.action),
                r,
              );
            }
          } else {
            // Something else.
            res[name] = r;
          }
        });

        return res;
      },

      /**
       * Create resolver for action
       *
       * @param {String} actionName
       * @param {Object?} def
       */
      createActionResolver(actionName, def = {}) {
        const {
          dataLoader = false,
          nullIfError = false,
          params = {},
          rootParams = {},
          meta = {},
          upload = false,
        } = def;
        const rootKeys = Object.keys(rootParams);

        return async (root, args, context) => {
          try {
            if (upload) {
              const file = await args[upload];

              if (
                typeof file === 'undefined' ||
                file === null ||
                typeof file.createReadStream !== 'function'
              ) {
                throw new Error('Invalid file provided');
              }

              delete args[upload];

              const stream = file.createReadStream();

              return await (context.ctx || context.service.broker).call(
                actionName,
                stream,
                {
                  meta: {
                    ...meta,
                    params: {
                      ...args,
                      filename: file.filename,
                    },
                  },
                },
              );
            }

            if (dataLoader) {
              const dataLoaderKey = rootKeys[0]; // use the first root key
              const rootValue = _.get(root, dataLoaderKey);
              if (rootValue == null) {
                return null;
              }

              const res = Array.isArray(rootValue)
                ? await Promise.all(
                    rootValue.map((item) =>
                      context.dataLoaders[actionName].load(item),
                    ),
                  )
                : await context.dataLoaders[actionName].load(rootValue);
              return res;
            }

            const p = {};

            if (root && rootKeys) {
              rootKeys.forEach((k) => _.set(p, rootParams[k], _.get(root, k)));
            }

            return await (context.ctx || context.service.broker).call(
              actionName,
              _.defaultsDeep(args, p, params),
              {
                meta: { ...meta },
              },
            );
          } catch (err) {
            if (nullIfError) {
              return null;
            }
            // if (err && err.ctx) {
            //   delete err.ctx; // Avoid circular JSON
            // }
            throw err;
          }
        };
      },

      /**
       * Create resolver for subscription
       *
       * @param {String} actionName
       * @param {Array?} tags
       * @param {Boolean?} filter
       */
      createAsyncIteratorResolver(actionName, tags = [], filter = false) {
        return {
          subscribe: filter
            ? withFilter(() => this.pubsub.asyncIterator(tags), filter)
            : () => this.pubsub.asyncIterator(tags),
          resolve: async (payload, params, context) =>
            this.broker.call(actionName, { ...params, payload }, context),
        };
      },

      /**
       * Generate GraphQL Schema
       *
       * @param {Object[]} services
       * @returns {Object} Generated schema
       */
      generateGraphQLSchema(services) {
        try {
          const typeDefs = [];
          let resolvers = {};

          if (mixinOptions.typeDefs) {
            typeDefs.push(mixinOptions.typeDefs);
          }

          if (mixinOptions.resolvers) {
            resolvers = _.cloneDeep(mixinOptions.resolvers);
          }

          const queries = [];
          const types = [];
          const mutations = [];
          const subscriptions = [];
          const interfaces = [];
          const unions = [];
          const enums = [];
          const inputs = [];

          const processedServices = new Set();

          services.forEach((service) => {
            const serviceName = this.getServiceName(service);

            // Skip multiple instances of services
            if (processedServices.has(serviceName)) return;
            processedServices.add(serviceName);

            if (service.settings.graphql) {
              const parseGlobalDef = (globalDef) => {
                if (globalDef.query) {
                  queries.push(globalDef.query);
                }

                if (globalDef.type) {
                  types.push(globalDef.type);
                }

                if (globalDef.mutation) {
                  mutations.push(globalDef.mutation);
                }

                if (globalDef.subscription) {
                  subscriptions.push(globalDef.subscription);
                }

                if (globalDef.interface) {
                  interfaces.push(globalDef.interface);
                }

                if (globalDef.union) {
                  unions.push(globalDef.union);
                }

                if (globalDef.enum) {
                  enums.push(globalDef.enum);
                }

                if (globalDef.input) {
                  inputs.push(globalDef.input);
                }

                if (globalDef.resolvers) {
                  _.forIn(globalDef.resolvers, (r, name) => {
                    resolvers[name] = _.merge(
                      resolvers[name] || {},
                      this.createActionResolvers(serviceName, r),
                    );
                  });
                }
              };

              // --- COMPILE SERVICE-LEVEL DEFINITIONS ---
              if (Array.isArray(service.settings.graphql)) {
                service.settings.graphql.forEach((def) => {
                  if ([].concat(def.project).includes(mixinOptions.project)) {
                    parseGlobalDef(def);
                  }
                });
              } else if (
                _.isObject(service.settings.graphql[mixinOptions.project])
              ) {
                parseGlobalDef(service.settings.graphql[mixinOptions.project]);
              }
            }

            // --- COMPILE ACTION-LEVEL DEFINITIONS ---
            const parseActionDef = (def, action) => {
              if (def.query) {
                const name = def.query.split(/[(:]/g)[0].trim();
                queries.push(def.query);
                if (!resolvers.Query) resolvers.Query = {};
                resolvers.Query[name] = this.createActionResolver(
                  action.name,
                  def,
                );
              }

              if (def.type) {
                types.push(def.type);
              }

              if (def.mutation) {
                const name = def.mutation.split(/[(:]/g)[0].trim();
                mutations.push(def.mutation);
                if (!resolvers.Mutation) resolvers.Mutation = {};
                resolvers.Mutation[name] = this.createActionResolver(
                  action.name,
                  def,
                );
              }

              if (def.subscription) {
                const name = def.subscription.split(/[(:]/g)[0].trim();
                subscriptions.push(def.subscription);
                if (!resolvers.Subscription) resolvers.Subscription = {};
                resolvers.Subscription[name] = this.createAsyncIteratorResolver(
                  action.name,
                  def.tags,
                  eval(def.filter),
                  def,
                );
              }

              if (def.interface) {
                interfaces.push(def.interface);
              }

              if (def.union) {
                unions.push(def.union);
              }

              if (def.enum) {
                enums.push(def.enum);
              }

              if (def.input) {
                inputs.push(def.input);
              }

              if (def.resolvers) {
                _.forIn(def.resolvers, (r, name) => {
                  resolvers[name] = _.merge(
                    resolvers[name] || {},
                    this.createActionResolvers(serviceName, r),
                  );
                });
              }
            };

            _.forIn(service.actions, (action) => {
              if (action.graphql) {
                if (Array.isArray(action.graphql)) {
                  action.graphql.forEach((def) => {
                    if ([].concat(def.project).includes(mixinOptions.project)) {
                      parseActionDef(def, action);
                    }
                  });
                } else if (_.isObject(action.graphql[mixinOptions.project])) {
                  parseActionDef(action.graphql[mixinOptions.project], action);
                }
              }
            });
          });

          let str = '';

          str += `
            type Query {
              _test_: Boolean
              ${queries.join('\n')}
            }
          `;

          if (
            types.length > 0 ||
            mutations.length > 0 ||
            subscriptions.length > 0 ||
            interfaces.length > 0 ||
            unions.length > 0 ||
            enums.length > 0 ||
            inputs.length > 0
          ) {
            if (types.length > 0) {
              str += `
								${types.join('\n')}
							`;
            }

            if (mutations.length > 0) {
              str += `
								type Mutation {
									${mutations.join('\n')}
								}
							`;
            }

            if (subscriptions.length > 0) {
              str += `
								type Subscription {
									${subscriptions.join('\n')}
								}
							`;
            }

            if (interfaces.length > 0) {
              str += `
								${interfaces.join('\n')}
							`;
            }

            if (unions.length > 0) {
              str += `
								${unions.join('\n')}
							`;
            }

            if (enums.length > 0) {
              str += `
								${enums.join('\n')}
							`;
            }

            if (inputs.length > 0) {
              str += `
								${inputs.join('\n')}
							`;
            }
          }

          typeDefs.push(str);

          return makeExecutableSchema({
            typeDefs,
            resolvers,
          });
        } catch (err) {
          throw new MoleculerServerError(
            'Unable to compile GraphQL schema',
            500,
            'UNABLE_COMPILE_GRAPHQL_SCHEMA',
            err,
          );
        }
      },

      prepareGraphQLSchema() {
        if (!shouldUpdateSchema && this.graphqlHandler) {
          // Schema is up-to-date
          return;
        }

        // Create new server & regenerate GraphQL schema
        this.logger.info(
          'Recreate Apollo GraphQL server and regenerate GraphQL schema...',
        );

        try {
          this.pubsub = new PubSub();
          // this.pubsub = new PubSub({
          //   host: config.PUBSUB_REDIS_HOST,
          //   port: config.PUBSUB_REDIS_PORT,
          //   retry_strategy: (options) => {
          //     // reconnect after upto 3000 milis
          //     return Math.max(options.attempt * 100, 3000);
          //   },
          // });
          // this.pubsub = new PubSub({
          //   host: '82.202.178.45',
          //   port: 32209,
          //   database: 'explosivebit_v9',
          //   user: 'explosivebit_douxmq348x',
          //   password: '2qtqKagrUEEakHRTBS62F',
          // });
          const services = this.broker.registry.getServiceList({
            withActions: true,
          });
          const schema = this.generateGraphQLSchema(services);

          this.logger.debug(
            `Generated GraphQL schema:\n\n${GraphQL.printSchema(schema)}`,
          );

          this.apolloServer = new ApolloServer(
            _.defaultsDeep({}, mixinOptions.serverOptions, {
              schema,
              context: async ({ req, connection, payload }) => {
                if (req) {
                  if (
                    req.headers.connection &&
                    req.headers.connection.toLowerCase() === 'upgrade'
                  )
                    return null;
                  const meta = await mixinOptions.meta.call(this, {
                    req,
                    ctx: req.$ctx,
                  });

                  Object.assign(req.$ctx.meta, meta);

                  return {
                    ctx: req.$ctx,
                    meta,
                    service: req.$service,
                    params: req.$params,
                    dataLoaders: this.createLoaders(req.$ctx, services),
                  };
                }

                return {
                  ...connection.context,
                  // dataLoaders: this.createLoaders(req.$ctx, services),
                };
              },
              subscriptions: {
                onConnect: async (params, socket, ctx) => {
                  this.logger.info('Subscription clinet connected:');
                  this.broker.broadcast('$ws.graphql.client', {
                    type: 'open',
                    headers: socket.upgradeReq.headers,
                  });
                  const meta = await mixinOptions.meta.call(this, {
                    connectionParams: params,
                  });
                  return {
                    meta,
                    service: this,
                    params,
                  };
                },
                onDisconnect: async (socket, ctx) => {
                  this.logger.info('Subscription clinet disconnected:');
                  this.broker.broadcast('$ws.graphql.client', {
                    type: 'close',
                    headers: socket.upgradeReq.headers,
                  });
                },
              },
            }),
          );

          this.graphqlHandler = this.apolloServer.createHandler();
          this.apolloServer.installSubscriptionHandlers(this.server);
          this.graphqlSchema = schema;

          shouldUpdateSchema = false;

          this.broker.broadcast('graphql.schema.updated', {
            schema: GraphQL.printSchema(schema),
          });
        } catch (err) {
          this.logger.error(err);
          throw err;
        }
      },

      /**
       * Get the name of a service including version spec
       * @param {Object} service - Service object
       * @returns {String} Name of service including version spec
       */
      getServiceName(service) {
        return service.version
          ? `v${service.version}.${service.name}`
          : service.name;
      },

      /**
       * Create the DataLoader instances to be used for batch resolution
       * @param {Object[]} services
       * @returns {Object.<string, Object>} Key/value pairs of DataLoader instances
       */
      createLoaders(ctx, services) {
        const loaders = services.reduce((serviceAccum, service) => {
          const serviceName = this.getServiceName(service);

          const { graphql } = service.settings;

          const createDataLoaders = (resolvers) => {
            const typeLoaders = Object.values(resolvers).reduce(
              (resolverAccum, type) => {
                const resolverLoaders = Object.values(type).reduce(
                  (fieldAccum, resolver) => {
                    if (_.isPlainObject(resolver)) {
                      const {
                        action,
                        dataLoader = false,
                        idKey = 'id',
                        primitive = false,
                        rootParams = {},
                        params = {},
                        meta = {},
                      } = resolver;
                      const actionParam = Object.values(rootParams)[0]; // use the first root parameter
                      if (dataLoader && actionParam) {
                        const resolverActionName = this.getResolverActionName(
                          serviceName,
                          action,
                        );
                        if (fieldAccum[resolverActionName] == null) {
                          // create a new DataLoader instance
                          fieldAccum[resolverActionName] = new DataLoader(
                            (keys) =>
                              ctx
                                .call(
                                  resolverActionName,
                                  {
                                    ...params,
                                    [actionParam]: keys,
                                  },
                                  { meta: { ...meta } },
                                )
                                .then((result) => {
                                  if (primitive) return result;
                                  const obj = {};

                                  result.forEach((r) => {
                                    if (!r) return;
                                    obj[r[idKey]] = r;
                                  });

                                  return keys.map((key) => obj[key] || null);
                                }),
                          );
                        }
                      }
                    }

                    return fieldAccum;
                  },
                  {},
                );

                return { ...resolverAccum, ...resolverLoaders };
              },
              {},
            );

            serviceAccum = { ...serviceAccum, ...typeLoaders };
          };

          if (graphql) {
            if (Array.isArray(graphql)) {
              graphql.forEach((def) => {
                if (![].concat(def.project).includes(mixinOptions.project))
                  return;
                if (!def.resolvers) return;
                createDataLoaders(def.resolvers);
              });
            } else if (
              _.isObject(graphql[mixinOptions.project]) &&
              graphql[mixinOptions.project].resolvers
            ) {
              createDataLoaders(graphql[mixinOptions.project].resolvers);
            }
          }

          _.forIn(service.actions, (action) => {
            if (action.graphql) {
              if (Array.isArray(action.graphql)) {
                action.graphql.forEach((def) => {
                  if (![].concat(def.project).includes(mixinOptions.project))
                    return;
                  if (!def.resolvers) return;
                  createDataLoaders(def.resolvers);
                });
              } else if (
                _.isObject(action.graphql[mixinOptions.project]) &&
                action.graphql[mixinOptions.project].resolvers
              ) {
                createDataLoaders(
                  action.graphql[mixinOptions.project].resolvers,
                );
              }
            }
          });

          return serviceAccum;
        }, {});

        return loaders;
      },
    },

    created() {
      this.apolloServer = null;
      this.graphqlHandler = null;
      this.pubsub = new PubSub();
      // this.pubsub = new PubSub({
      //   host: config.PUB_SUB_HOST,
      //   port: config.PUB_SUB_PORT,
      // });
      // this.pubsub = new PubSub({
      //   host: '82.202.178.45',
      //   port: 32209,
      //   database: 'explosivebit_v9',
      //   user: 'explosivebit_douxmq348x',
      //   password: '2qtqKagrUEEakHRTBS62F',
      // });

      const route = _.defaultsDeep(mixinOptions.routeOptions, {
        aliases: {
          '/': function (req, res) {
            try {
              this.prepareGraphQLSchema();
              return this.graphqlHandler(req, res);
            } catch (err) {
              this.sendError(req, res, err);
            }
          },
        },
        mappingPolicy: 'restrict',
        bodyParsers: {
          json: true,
          urlencoded: { extended: true },
        },
      });

      // Add route
      this.settings.routes.unshift(route);
    },

    started() {
      this.logger.info(
        `GraphQL server is available at ${mixinOptions.routeOptions.path}`,
      );
    },
  };

  if (mixinOptions.createAction) {
    serviceSchema.actions = {
      graphql: {
        params: {
          query: { type: 'string' },
          variables: { type: 'object', optional: true },
        },
        handler(ctx) {
          this.prepareGraphQLSchema();

          ctx.meta.device = {
            id: 'system',
          };

          const services = this.broker.registry.getServiceList({
            withActions: true,
          });

          return GraphQL.graphql(
            this.graphqlSchema,
            ctx.params.query,
            null,
            {
              ctx,
              service: this,
              dataLoaders: this.createLoaders(ctx, services),
            },
            ctx.params.variables,
          );
        },
      },
    };
  }
  return serviceSchema;
};
