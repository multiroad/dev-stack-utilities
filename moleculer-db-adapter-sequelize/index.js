/*
 * moleculer-db-adapter-sequelize
 * Copyright (c) 2019 MoleculerJS (https://github.com/moleculerjs/moleculer-db)
 * MIT Licensed
 */

'use strict';

const _ = require('lodash');
const { ServiceSchemaError } = require('moleculer').Errors;
const Promise = require('bluebird');
const Sequelize = require('sequelize');

const { Model, Op } = Sequelize;

class SequelizeDbAdapter {

  /**
   * Creates an instance of SequelizeDbAdapter.
   * @param {any} opts
   *
   * @memberof SequelizeDbAdapter
   */
  constructor(...opts) {
    this.opts = opts;
  }

  /**
   * Initialize adapter
   *
   * @param {ServiceBroker} broker
   * @param {Service} service
   *
   * @memberof SequelizeDbAdapter
   */
  init(broker, service) {
    this.broker = broker;
    this.service = service;

    if (!this.service.schema.model) {
      /* istanbul ignore next */
      throw new ServiceSchemaError('Missing `model` definition in schema of service!');
    }
  }

  /**
   * Connect to database
   *
   * @returns {Promise}
   *
   * @memberof SequelizeDbAdapter
   */
  connect() {
    const sequelizeInstance = this.opts[0];

    const operatorsAliases = {
      _eQ: Op.eq,
      _nE: Op.ne,
      _gtE: Op.gte,
      _gT: Op.gt,
      _ltE: Op.lte,
      _lT: Op.lt,
      _noT: Op.not,
      _iN: Op.in,
      _notIN: Op.notIn,
      _iS: Op.is,
      _likE: Op.like,
      _notLikE: Op.notLike,
      _iLikE: Op.iLike,
      _notILikE: Op.notILike,
      _regexP: Op.regexp,
      _notRegexP: Op.notRegexp,
      _iRegexP: Op.iRegexp,
      _notIRegexP: Op.notIRegexp,
      _betweeN: Op.between,
      _notBetweeN: Op.notBetween,
      _overlaP: Op.overlap,
      _containS: Op.contains,
      _containeD: Op.contained,
      _adjacenT: Op.adjacent,
      _strictLefT: Op.strictLeft,
      _strictRighT: Op.strictRight,
      _noExtendRighT: Op.noExtendRight,
      _noExtendLefT: Op.noExtendLeft,
      _anD: Op.and,
      _oR: Op.or,
      _anY: Op.any,
      _alL: Op.all,
      _valueS: Op.values,
      _coL: Op.col,
      _Fn: Op._Fn,
    };

    const objOptions = this.opts.pop();
    const newOptions = [...this.opts];

    if (_.isObject(objOptions)) {
      newOptions.push({
        ...objOptions,
        operatorsAliases: {
          ...Object.assign(objOptions.operatorsAliases || {}, operatorsAliases),
        },
      });
    }

    if (sequelizeInstance && sequelizeInstance instanceof Sequelize) {
      this.db = sequelizeInstance;
    } else {
      this.db = new Sequelize(...newOptions);
    }

    return this.db.authenticate().then(() => {
      this.modelsArray = [].concat(
        this.service.schema.model || this.service.schema.models,
      );
      this.models = {};

      const modelsReadyPromises = [];

      this.modelsArray.forEach(model => {
        this.models[model.name] = this.db.define(
          model.name,
          model.define,
          model.options,
        );
        modelsReadyPromises.push(this.models[model.name].sync());
      });

      Object.values(this.models).forEach(model => {
        if (
          model.options.classMethods &&
          model.options.classMethods.associate
        ) {
          model.options.classMethods.associate.call(model, this);
        }
      });

      if (
        this.modelsArray.length > 1 &&
        !this.service.schema.settings.modelName
      ) {
        throw new Error('Main model name has not found');
      } else {
        this.modelName =
          this.service.schema.settings.modelName || this.modelsArray[0].name;
      }

      this.model = this.models[this.modelName];

      this.service.model = this.model;
      this.service.models = this.models;

      return Promise.all(modelsReadyPromises).then(() => {
        this.service.logger.info(
          'Sequelize adapter has connected successfully.',
        );
      });
    });
  }

  /**
   * Disconnect from database
   *
   * @returns {Promise}
   *
   * @memberof SequelizeDbAdapter
   */
  disconnect() {
    if (this.db) {
      return this.db.close();
    }
    /* istanbul ignore next */
    return Promise.resolve();
  }

  /**
   * Find all entities by filters.
   *
   * Available filter props:
   *  - limit
   *  - offset
   *  - sort
   *  - search
   *  - searchFields
   *  - query
   *
   * @param {any} filters
   * @param {Object} transaction
   * @returns {Promise}
   *
   * @memberof SequelizeDbAdapter
   */
  find(filters, transaction) {
    return this.createCursor(filters, false, transaction);
  }

  /**
   * Find an entity by query
   *
   * @param {Object} query
   * @param {Object} transaction
   * @returns {Promise}
   * @memberof MemoryDbAdapter
   */
  findOne(query, transaction) {
    if (transaction) {
      return this.model.findOne({
        ...query,
        transaction,
      });
    }
    return this.model.findOne(query);
  }

  /**
   * Find an entities by ID
   *
   * @param {any} _id
   * @param {Object} transaction
   * @returns {Promise}
   *
   * @memberof SequelizeDbAdapter
   */
  findById(_id, transaction) {
    if (transaction) {
      return this.model.findByPk(_id, { transaction });
    }
    return this.model.findByPk(_id);
  }

  /**
   * Find any entities by IDs
   *
   * @param {Array} idList
   * @param {Object} transaction
   * @returns {Promise}
   *
   * @memberof SequelizeDbAdapter
   */
  findByIds(idList, transaction) {
    const options = {
      where: {
        id: {
          [Op.in]: idList,
        },
      },
    };

    if (transaction) {
      options.transaction = transaction;
    }

    return this.model.findAll(options);
  }

  /**
   * Get count of filtered entities
   *
   * Available filter props:
   *  - search
   *  - searchFields
   *  - query
   *
   * @param {Object} [filters={}]
   * @returns {Promise}
   *
   * @memberof SequelizeDbAdapter
   */
  count(filters = {}, transaction) {
    return this.createCursor(filters, true, transaction);
  }

  /**
   * Insert an entity
   *
   * @param {Object} entity
   * @returns {Promise}
   *
   * @memberof SequelizeDbAdapter
   */
  insert(entity, transaction) {
    return this.model.create(entity, {
      transaction,
    });
  }

  /**
   * Insert an entity
   *
   * @param {Object} entity
   * @returns {Promise}
   *
   * @memberof SequelizeDbAdapter
   */
  upsert(entity, fields, transaction) {
    return this.model.upsert(entity, {
      transaction,
      fields,
    });
  }

  /**
   * Insert many entities
   *
   * @param {Array} entities
   * @param {Object} transaction
   * @returns {Promise}
   *
   * @memberof SequelizeDbAdapter
   */
  insertMany(entities, transaction) {
    return this.model.bulkCreate(entities, { returning: true, transaction });
  }

  /**
   * Upsert many entities
   *
   * @param {Array} entities
   * @param {Object} transaction
   * @returns {Promise}
   *
   * @memberof SequelizeDbAdapter
   */
  upsertMany(entities, fields, transaction) {
    return this.model.bulkCreate(entities, { returning: true, updateOnDuplicate: fields || [...this.service.settings.fields], transaction });
  }

  /**
   * Update many entities by `where` and `update`
   *
   * @param {Object} where
   * @param {Object} update
   * @param {Object} transaction
   * @returns {Promise}
   *
   * @memberof SequelizeDbAdapter
   */
  updateMany(where, update, transaction) {
    return this.model.update(update, { where, transaction }).then(res => res[0]);
  }

  /**
   * Update an entity by ID and `update`
   *
   * @param {any} _id
   * @param {Object} update
   * @param {Object} transaction
   * @returns {Promise}
   *
   * @memberof SequelizeDbAdapter
   */
  updateById(_id, update, transaction) {
    return this.findById(_id, transaction).then(entity => {
      return entity && entity.update(update['$set'], {
        transaction,
      });
    });
  }

  /**
   * Remove entities which are matched by `where`
   *
   * @param {Object} where
   * @param {Object} transaction
   * @returns {Promise}
   *
   * @memberof SequelizeDbAdapter
   */
  removeMany(where, transaction) {
    return this.model.destroy({ where, transaction });
  }

  /**
   * Remove an entity by ID
   *
   * @param {any} _id
   * @param {Object} transaction
   * @returns {Promise}
   *
   * @memberof SequelizeDbAdapter
   */
  removeById(_id, transaction) {
    return this.findById(_id, transaction).then(entity => {
      return entity && entity.destroy({ transaction }).then(() => entity);
    });
  }

  /**
   * Clear all entities from collection
   *
   * @returns {Promise}
   *
   * @memberof SequelizeDbAdapter
   */
  clear(transaction) {
    return this.model.destroy({ where: {}, transaction });
  }

  /**
   * Convert DB entity to JSON object
   *
   * @param {any} entity
   * @returns {Object}
   * @memberof SequelizeDbAdapter
   */
  entityToObject(entity) {
    return entity.get({ plain: true });
  }

  /**
   * Create a filtered query
   * Available filters in `params`:
   *  - search
   *  - sort
   *  - limit
   *  - offset
   *  - query
   *
   * @param {Object} params
   * @param {Boolean} isCounting
   * @param {Object} transaction
   * @returns {Promise}
   */
  createCursor(params, isCounting, transaction) {
    if (!params) {
      if (isCounting) {
        return this.model.count({ transaction });
      }

      return this.model.findAll({
        transaction,
      });
    }

    const q = {
      where: params.query || {},
      transaction,
    };

    // Text search
    if (
      (_.isString(params.search) || _.isArray(params.search)) &&
      params.search !== ''
    ) {
      let fields = [];
      if (params.searchFields) {
        fields = _.isString(params.searchFields)
          ? params.searchFields.split(' ')
          : params.searchFields;
      }

      q.where = {
        _anD: [
          {
            ...q.where,
          },
        ],
      };

      const typeLike = params._case ? '_likE' : '_iLikE';
      const search = [];

      if (_.isArray(params.search)) {
        params.search.forEach(r => {
          search.push(
            ...fields.map(f => ({
              [f]: {
                [typeLike]: `%${r}%`,
              },
            })),
          );
        });
      } else {
        search.push(
          ...fields.map(f => ({
            [f]: {
              [typeLike]: `%${params.search}%`,
            },
          })),
        );
      }
      q.where._oR = search;
    }

    // Sort
    if (params.sort) {
      const sort = this.transformSort(params.sort);
      if (sort) q.order = sort;
    }

    // Include
    if (params.include) {
      q.include = params.include;
    }

    // Attributes
    if (params.attributes) {
      q.attributes = params.attributes;
    }

    // Offset
    if (_.isNumber(params.offset) && params.offset > 0) {
      q.offset = params.offset;
    }

    // Limit
    if (_.isNumber(params.limit) && params.limit > 0) q.limit = params.limit;

    if (isCounting) return this.model.count(q);

    return this.model.findAll(q);
  }

  /**
   * Convert the `sort` param to a `sort` object to Sequelize queries.
   *
   * @param {String|Array<String>|Object} paramSort
   * @returns {Object} Return with a sort object like `[["votes", "ASC"], ["title", "DESC"]]`
   * @memberof SequelizeDbAdapter
   */
  transformSort(paramSort) {
    let sort = paramSort;
    if (_.isString(sort)) {
      sort = sort.replace(/,/, ' ').split(' ');
    }

    if (Array.isArray(sort)) {
      let sortObj = [];
      sort.forEach(s => {
        if (s.startsWith('-')) {
          sortObj.push([s.slice(1), 'DESC']);
        } else {
          sortObj.push([s, 'ASC']);
        }
      });
      return sortObj;
    }

    if (_.isObject(sort)) {
      return Object.keys(sort).map(name => [name, sort[name] > 0 ? 'ASC' : 'DESC']);
    }

    /* istanbul ignore next*/
    return [];
  }

  /**
   * For compatibility only.
   * @param {Object} entity
   * @param {String} idField
   * @memberof SequelizeDbAdapter
   * @returns {Object} Entity
   */
  beforeSaveTransformID(entity, idField) {
    return entity;
  }

  /**
   * For compatibility only.
   * @param {Object} entity
   * @param {String} idField
   * @memberof SequelizeDbAdapter
   * @returns {Object} Entity
   */
  afterRetrieveTransformID(entity, idField) {
    return entity;
  }

}

module.exports = SequelizeDbAdapter;
