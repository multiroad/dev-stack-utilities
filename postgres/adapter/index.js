import { Pool } from 'pg';

export default class PostgresDbAdapter {
  constructor(opts) {
    this.opts = opts;
  }

  init(broker, service) {
    this.broker = broker;
    this.service = service;

    this.pool = new Pool({
      host: this.opts.host, // 'localhost' is the default;
      port: this.opts.port, // 5432 is the default;
      database: this.opts.database,
      user: this.opts.username,
      password: this.opts.password,
      max: 5,
    });

    this.client = null;

    return this;
  }

  connect() {
    return this.pool.connect();
  }

  async disconnect() {
    if (this.pool) {
      await this.pool.end();
    }
    return Promise.resolve();
  }

  async query(...args) {
    if (!this.client) {
      this.client = await this.connect();
    }
    return this.client.query(...args);
  }
}
