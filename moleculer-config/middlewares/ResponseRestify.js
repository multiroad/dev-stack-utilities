import http from 'http';
import { Readable } from 'stream';
import streamToBuffer from '../../util/streamBuffer/streamToBuffer';

export default function (broker) {
  const requests = {};

  const srv = http.createServer((req, res) => {
    const ctxId = req.url.slice(1);

    broker.logger.debug('Requested response stream', ctxId);

    if (!requests[ctxId]) {
      broker.logger.warn('Response stream', ctxId, 'was not found');
      res.writeHead(404);
      return res.end();
    }

    broker.logger.debug('Piping response stream', ctxId);

    requests[ctxId].pipe(res);
    delete requests[ctxId];
  });

  srv.listen(process.env.RESPONSE_RESTIFY_PORT || 0, '0.0.0.0', () => {
    const address = srv.address();
    broker.logger.info('ResponseRestify server is listening on', `http://${address.address}:${address.port}`);
  });

  const restify = (ctx, stream, toJSON = false) => {
    const address = srv.address();

    const host = process.env.RESPONSE_RESTIFY_HOST || broker.registry.nodes.localNode.ipList[0];
    const port = address.port;

    ctx.meta.$Restify_response = true;
    ctx.meta.$Restify_url = `http://${host}:${port}/${ctx.id}`;
    ctx.meta.$Restify_toJSON = toJSON;

    requests[ctx.id] = stream;

    return null;
  };

  return {
    localAction(handler) {
      return ctx => {
        // Skip ResponseRestify if action was called by a local node
        if (ctx.nodeID === broker.nodeID) return handler(ctx);

        return handler(ctx)
          .then(response => {
            // Return response if it is undefined, null, false or empty string
            if (!response) return response;

            const isStream = response && response.readable === true && typeof response.on === 'function' && typeof response.pipe === 'function';

            if (isStream) return restify(ctx, response);

            // TODO: Find a way to not stringify data twice
            const stringified = JSON.stringify(response);

            const buf = Buffer.from(stringified, 'utf8');

            const maxChunkSize = +process.env.RESPONSE_RESTIFY_RIM || 1044480; // 1020KB

            if (buf.length < maxChunkSize) {
              // If data size is less than {maxChunkSize} - just return a response
              return response;
            }

            const stream = new Readable();

            stream.push(buf);
            stream.push(null);

            return restify(ctx, stream, true);
          });
      };
    },
    remoteAction(handler, action) {
      return ctx => {
        return handler(ctx)
          .then(async (response) => {
            if (!ctx.meta.$Restify_response) return response;

            const span = ctx.startSpan(`action '${action.name}' ResponseRestify`);

            try {
              const url = ctx.meta.$Restify_url;

              if (!url) {
                throw new Error(`URL is required in order to get the restified response of the request #${ctx.id}`);
              }

              const toJSON = ctx.meta.$Restify_toJSON || false;

              delete ctx.meta.$Restify_response;
              delete ctx.meta.$Restify_url;
              delete ctx.meta.$Restify_toJSON;

              return await new Promise((resolve, reject) => {
                const handleError = err => reject(new Error(`Failed to load response of ${action.name} (${ctx.id})\n${err}`));

                const request = http.get(url, (res) => {
                  if (res.statusCode !== 200) {
                    return handleError(new Error(`Unexpected response status code ${res.statusCode}`));
                  }

                  if (toJSON) {
                    return streamToBuffer(res)
                      .then(JSON.parse)
                      .then(resolve)
                      .catch(handleError);
                  }

                  return resolve(res);
                });

                request.on('error', handleError);
              });
            } catch (err) {
              span.setError(err);
              throw err;
            } finally {
              ctx.finishSpan(span);
            }
          });
      };
    },
  };
};
