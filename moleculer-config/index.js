import { inspect } from 'util';
import defaultsDeep from 'lodash.defaultsdeep';
// TODO: придумать как вернуть ResponseValidator

import ResponseValidator from './middlewares/ResponseValidator';
import ResponseRestify from './middlewares/ResponseRestify';
import ResponseStreamify from './middlewares/ResponseStreamify';
import ActionLogger from './middlewares/ActionLogger';

import logLevel from './logLevel';
import tracing from './tracing';

export default config => defaultsDeep(config, {
  namespace: 'development',
  nodeID: `project-service:${String(Date.now()).slice(-5)}`,
  // Logger
  logger: [{
    type: 'Console',
    options: {
      // Logging level
      // level: "info",
      // Using colors on the output
      colors: true,
      // Print module names with different colors (like docker-compose for containers)
      moduleColors: true,
      // Line formatter. It can be "json", "short", "simple", "full", a `Function` or a template string like "{timestamp} {level} {nodeID}/{mod}: {msg}"
      formatter: 'short',
      // Custom object printer. If not defined, it uses the `util.inspect` method.
      objectPrinter: o => inspect(o, { colors: true, depth: process.env.LOG_DEPTH || 1 }),
      // Auto-padding the module name in order to messages begin at the same column.
      autoPadding: false,
    },
  }],
  logLevel,
  //
  registry: {
    strategy: 'RoundRobin',
    preferLocal: true,
  },
  validator: true,
  bulkhead: {
    enabled: false,
    concurrency: 10,
    maxQueueSize: 100,
  },
  metrics: false,
  tracing,
  statistics: false,
  circuitBreaker: {
    enabled: true,
    maxFailures: 1,
    halfOpenTime: 10 * 1000,
    failureOnTimeout: true,
    failureOnReject: true,
  },
  transporter: {
    type: 'NATS',
    options: {
      url: 'nats://localhost:4222',
      user: '',
      pass: '',
    },
  },
  cacher: 'Memory',
  requestTimeout: 20 * 1000,
  requestRetry: 3,
  created(broker) {
    const prevCount = broker.middlewares.count();

    // 12. ResponseRestify
    broker.middlewares.add(ResponseRestify);
    // broker.middlewares.add(ResponseStreamify);

    // 15. Action Logger
    broker.middlewares.add(ActionLogger);

    broker.logger.info(`Registered ${broker.middlewares.count() - prevCount} additional internal middleware(s).`);
  },
});
