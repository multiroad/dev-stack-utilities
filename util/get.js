export default (obj, path, defaultValue) => {
  const paths = path.split('.');

  let current = obj;
  let i;

  for (i = 0; i < paths.length; i++) {
    const currPathPiece = paths[i];
    if (
      typeof current !== 'object'
      || current === null
      || typeof current[currPathPiece] === 'undefined'
    ) {
      return defaultValue;
    } else {
      current = current[currPathPiece];
    }
  }
  return current;
};
