import crypto from 'crypto';

const secret = 'project';

export default str => crypto.createHmac('sha256', secret).update(str, 'utf8').digest('hex');
