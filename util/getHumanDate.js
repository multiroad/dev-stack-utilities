const months = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
];

export default (value, {
  time = false, timeSeparator = ':', dateSeparator = '.', dateTimeSeparator = ' ',
  monthWord = false,
} = {}) => {
  const date = new Date(+value);

  const day = String(date.getUTCDate()).padStart(2, '0');
  const year = date.getUTCFullYear();
  const hours = String(date.getUTCHours()).padStart(2, '0');
  const minutes = String(date.getUTCMinutes()).padStart(2, '0');
  const seconds = String(date.getUTCSeconds()).padStart(2, '0');

  if (monthWord) {
    if (time) {
      return `${day} ${months[date.getUTCMonth()]} ${year}${dateTimeSeparator}${hours}${timeSeparator}${minutes}${timeSeparator}${seconds}`;
    }

    return `${day} ${months[date.getUTCMonth()]} ${year}`;
  }

  const month = String(date.getUTCMonth() + 1).padStart(2, '0');

  if (time) {
    return `${day}${dateSeparator}${month}${dateSeparator}${year}${dateTimeSeparator}${hours}${timeSeparator}${minutes}${timeSeparator}${seconds}`;
  }
  return `${day}${dateSeparator}${month}${dateSeparator}${year}`;
};
