import { DataTypes, Utils, Op } from 'sequelize';
import AbstractQueryGenerator from 'sequelize/lib/dialects/abstract/query-generator';
import Model from 'sequelize/lib/model';
import sequelizeErrors from 'sequelize/lib/errors';
import Promise from 'sequelize/lib/promise';
import BelongsTo from 'sequelize/lib/associations/belongs-to';
import BelongsToMany from 'sequelize/lib/associations/belongs-to-many';
import _ from 'lodash';

const dialects = new Set(['mariadb', 'mysql', 'postgres', 'sqlite', 'mssql']);

Utils.now = function now(dialect) {
  const d = new Date();
  if (!dialects.has(dialect)) {
    d.setMilliseconds(0);
  }
  return +d;
};

// Fix for where clause with GEOJSON
AbstractQueryGenerator.prototype._whereBind = function _whereBind(binding, key, value, options) {
  const field = this._findField(key, options);
  const fieldType = field && field.type || options.type;

  if (fieldType instanceof DataTypes.GEOMETRY) {
    return this._whereParseSingleValueObject(key, field, this.OperatorMap[Op.eq], value, options);
  }

  if (_.isPlainObject(value)) {
    value = Utils.getComplexKeys(value).map(prop => {
      const item = value[prop];
      return this.whereItemQuery(key, { [prop]: item }, options);
    });
  } else {
    value = value.map(item => this.whereItemQuery(key, item, options));
  }

  value = value.filter(item => item && item.length);

  return value.length ? `(${value.join(binding)})` : undefined;
};

Model.bulkCreate = function bulkCreate(records, options = {}) {
  if (!records.length) {
    return Promise.resolve([]);
  }

  const dialect = this.sequelize.options.dialect;
  const now = Utils.now(this.sequelize.options.dialect);

  options.model = this;

  if (!options.includeValidated) {
    this._conformIncludes(options, this);
    if (options.include) {
      this._expandIncludeAll(options);
      this._validateIncludedElements(options);
    }
  }

  const instances = records.map(values => this.build(values, { isNewRecord: true, include: options.include }));

  const recursiveBulkCreate = (instances, options) => {
    options = Object.assign({
      validate: false,
      hooks: true,
      individualHooks: false,
      ignoreDuplicates: false,
    }, options);

    if (options.returning === undefined) {
      if (options.association) {
        options.returning = false;
      } else {
        options.returning = true;
      }
    }

    if (options.ignoreDuplicates && ['mssql'].includes(dialect)) {
      return Promise.reject(new Error(`${dialect} does not support the ignoreDuplicates option.`));
    }
    if (options.updateOnDuplicate && (dialect !== 'mysql' && dialect !== 'mariadb' && dialect !== 'sqlite' && dialect !== 'postgres')) {
      return Promise.reject(new Error(`${dialect} does not support the updateOnDuplicate option.`));
    }

    const model = options.model;

    options.fields = options.fields || Object.keys(model.rawAttributes);
    const createdAtAttr = model._timestampAttributes.createdAt;
    const updatedAtAttr = model._timestampAttributes.updatedAt;

    if (options.updateOnDuplicate !== undefined) {
      if (Array.isArray(options.updateOnDuplicate) && options.updateOnDuplicate.length) {
        options.updateOnDuplicate = _.intersection(
          _.without(Object.keys(model.tableAttributes), createdAtAttr),
          options.updateOnDuplicate,
        );
      } else {
        return Promise.reject(new Error('updateOnDuplicate option only supports non-empty array.'));
      }
    }

    return Promise.try(() => {
      // Run before hook
      if (options.hooks) {
        return model.runHooks('beforeBulkCreate', instances, options);
      }
    }).then(() => {
      // Validate
      if (options.validate) {
        const errors = new Promise.AggregateError();
        const validateOptions = _.clone(options);
        validateOptions.hooks = options.individualHooks;

        return Promise.map(instances, instance =>
          instance.validate(validateOptions).catch(err => {
            errors.push(new sequelizeErrors.BulkRecordError(err, instance));
          }),
        ).then(() => {
          delete options.skip;
          if (errors.length) {
            throw errors;
          }
        });
      }
    }).then(() => {
      if (options.individualHooks) {
        // Create each instance individually
        return Promise.map(instances, instance => {
          const individualOptions = _.clone(options);
          delete individualOptions.fields;
          delete individualOptions.individualHooks;
          delete individualOptions.ignoreDuplicates;
          individualOptions.validate = false;
          individualOptions.hooks = true;

          return instance.save(individualOptions);
        });
      }

      return Promise.resolve().then(() => {
        if (!options.include || !options.include.length) return;

        // Nested creation for BelongsTo relations
        return Promise.map(options.include.filter(include => include.association instanceof BelongsTo), include => {
          const associationInstances = [];
          const associationInstanceIndexToInstanceMap = [];

          for (const instance of instances) {
            const associationInstance = instance.get(include.as);
            if (associationInstance) {
              associationInstances.push(associationInstance);
              associationInstanceIndexToInstanceMap.push(instance);
            }
          }

          if (!associationInstances.length) {
            return;
          }

          const includeOptions = _(Utils.cloneDeep(include))
            .omit(['association'])
            .defaults({
              transaction: options.transaction,
              logging: options.logging,
            }).value();

          return recursiveBulkCreate(associationInstances, includeOptions).then(associationInstances => {
            for (const idx in associationInstances) {
              const associationInstance = associationInstances[idx];
              const instance = associationInstanceIndexToInstanceMap[idx];

              instance[include.association.accessors.set](associationInstance, { save: false, logging: options.logging });
            }
          });
        });
      }).then(() => {
        // Create all in one query
        // Recreate records from instances to represent any changes made in hooks or validation
        records = instances.map(instance => {
          const values = instance.dataValues;

          // set createdAt/updatedAt attributes
          if (createdAtAttr && !values[createdAtAttr]) {
            values[createdAtAttr] = now;
            if (!options.fields.includes(createdAtAttr)) {
              options.fields.push(createdAtAttr);
            }
          }
          if (updatedAtAttr && !values[updatedAtAttr]) {
            values[updatedAtAttr] = now;
            if (!options.fields.includes(updatedAtAttr)) {
              options.fields.push(updatedAtAttr);
            }
          }

          const out = Object.assign({}, Utils.mapValueFieldNames(values, options.fields, model));
          for (const key of model._virtualAttributes) {
            delete out[key];
          }
          return out;
        });

        // Map attributes to fields for serial identification
        const fieldMappedAttributes = {};
        for (const attr in model.tableAttributes) {
          fieldMappedAttributes[model.rawAttributes[attr].field || attr] = model.rawAttributes[attr];
        }

        // Map updateOnDuplicate attributes to fields
        if (options.updateOnDuplicate) {
          options.updateOnDuplicate = options.updateOnDuplicate.map(attr => model.rawAttributes[attr].field || attr);
          // Get primary keys for postgres to enable updateOnDuplicate
          options.upsertKeys = _.chain(model.primaryKeys).values().map('field').value();
          if (Object.keys(model.uniqueKeys).length > 0) {
            if (options.duplicateConstraint && model.uniqueKeys[options.duplicateConstraint]) {
              options.upsertKeys = [...model.uniqueKeys[options.duplicateConstraint].fields];
            } else {
              options.upsertKeys = _.chain(model.uniqueKeys).values().filter(c => c.fields.length === 1).map(c => c.fields[0]).value();
            }
          }
        }

        // Map returning attributes to fields
        if (options.returning && Array.isArray(options.returning)) {
          options.returning = options.returning.map(attr => model.rawAttributes[attr].field || attr);
        }

        return model.QueryInterface.bulkInsert(model.getTableName(options), records, options, fieldMappedAttributes).then(results => {
          if (Array.isArray(results)) {
            results.forEach((result, i) => {
              const instance = instances[i];

              for (const key in result) {
                if (!instance || key === model.primaryKeyAttribute &&
                  instance.get(model.primaryKeyAttribute) &&
                  ['mysql', 'mariadb', 'sqlite'].includes(dialect)) {
                  // The query.js for these DBs is blind, it autoincrements the
                  // primarykey value, even if it was set manually. Also, it can
                  // return more results than instances, bug?.
                  continue;
                }
                if (Object.prototype.hasOwnProperty.call(result, key)) {
                  const record = result[key];

                  const attr = _.find(model.rawAttributes, attribute => attribute.fieldName === key || attribute.field === key);

                  instance.dataValues[attr && attr.fieldName || key] = record;
                }
              }
            });
          }
          return results;
        });
      });
    }).then(() => {
      if (!options.include || !options.include.length) return;

      // Nested creation for HasOne/HasMany/BelongsToMany relations
      return Promise.map(options.include.filter(include => !(include.association instanceof BelongsTo ||
        include.parent && include.parent.association instanceof BelongsToMany)), include => {
        const associationInstances = [];
        const associationInstanceIndexToInstanceMap = [];

        for (const instance of instances) {
          let associated = instance.get(include.as);
          if (!Array.isArray(associated)) associated = [associated];

          for (const associationInstance of associated) {
            if (associationInstance) {
              if (!(include.association instanceof BelongsToMany)) {
                associationInstance.set(include.association.foreignKey, instance.get(include.association.sourceKey || instance.constructor.primaryKeyAttribute, { raw: true }), { raw: true });
                Object.assign(associationInstance, include.association.scope);
              }
              associationInstances.push(associationInstance);
              associationInstanceIndexToInstanceMap.push(instance);
            }
          }
        }

        if (!associationInstances.length) {
          return;
        }

        const includeOptions = _(Utils.cloneDeep(include))
          .omit(['association'])
          .defaults({
            transaction: options.transaction,
            logging: options.logging,
          }).value();

        return recursiveBulkCreate(associationInstances, includeOptions).then(associationInstances => {
          if (include.association instanceof BelongsToMany) {
            const valueSets = [];

            for (const idx in associationInstances) {
              const associationInstance = associationInstances[idx];
              const instance = associationInstanceIndexToInstanceMap[idx];

              const values = {};
              values[include.association.foreignKey] = instance.get(instance.constructor.primaryKeyAttribute, { raw: true });
              values[include.association.otherKey] = associationInstance.get(associationInstance.constructor.primaryKeyAttribute, { raw: true });

              // Include values defined in the association
              Object.assign(values, include.association.through.scope);
              if (associationInstance[include.association.through.model.name]) {
                for (const attr of Object.keys(include.association.through.model.rawAttributes)) {
                  if (include.association.through.model.rawAttributes[attr]._autoGenerated ||
                    attr === include.association.foreignKey ||
                    attr === include.association.otherKey ||
                    typeof associationInstance[include.association.through.model.name][attr] === undefined) {
                    continue;
                  }
                  values[attr] = associationInstance[include.association.through.model.name][attr];
                }
              }

              valueSets.push(values);
            }

            const throughOptions = _(Utils.cloneDeep(include))
              .omit(['association', 'attributes'])
              .defaults({
                transaction: options.transaction,
                logging: options.logging,
              }).value();
            throughOptions.model = include.association.throughModel;
            const throughInstances = include.association.throughModel.bulkBuild(valueSets, throughOptions);

            return recursiveBulkCreate(throughInstances, throughOptions);
          }
        });
      });
    }).then(() => {
      // map fields back to attributes
      instances.forEach(instance => {
        for (const attr in model.rawAttributes) {
          if (model.rawAttributes[attr].field &&
            instance.dataValues[model.rawAttributes[attr].field] !== undefined &&
            model.rawAttributes[attr].field !== attr
          ) {
            instance.dataValues[attr] = instance.dataValues[model.rawAttributes[attr].field];
            delete instance.dataValues[model.rawAttributes[attr].field];
          }
          instance._previousDataValues[attr] = instance.dataValues[attr];
          instance.changed(attr, false);
        }
        instance.isNewRecord = false;
      });

      // Run after hook
      if (options.hooks) {
        return model.runHooks('afterBulkCreate', instances, options);
      }
    }).then(() => instances);
  };

  return recursiveBulkCreate(instances, options);
};

// Model.destroy = function destroy(options) {
//   options = Object.assign({
//     hooks: true,
//     force: false
//   }, options);
//
//   return Promise.try(() => {
//     // Run before hook
//     if (options.hooks) {
//       return this.runHooks('beforeDestroy', this, options);
//     }
//   }).then(() => {
//     const where = this.where(true);
//
//     if (this._timestampAttributes.deletedAt && options.force === false) {
//       const attributeName = this._timestampAttributes.deletedAt;
//       const attribute = this.rawAttributes[attributeName];
//       const defaultValue = Object.prototype.hasOwnProperty.call(attribute, 'defaultValue')
//         ? attribute.defaultValue
//         : null;
//       const currentValue = this.getDataValue(attributeName);
//       const undefinedOrNull = currentValue == null && defaultValue == null;
//       if (undefinedOrNull || _.isEqual(currentValue, defaultValue)) {
//         // only update timestamp if it wasn't already set
//         this.setDataValue(attributeName, +new Date());
//       }
//
//       return this.save(_.defaults({ hooks: false }, options));
//     }
//     return this.QueryInterface.delete(this, this.getTableName(options), where, Object.assign({ type: QueryTypes.DELETE, limit: null }, options));
//   }).tap(() => {
//     // Run after hook
//     if (options.hooks) {
//       return this.runHooks('afterDestroy', this, options);
//     }
//   });
// }
